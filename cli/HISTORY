Version Release History
=======================


Version 2.0 - August 27, 2014
-----------------------------

    * New mode: multi-sample BAM QC
    * Counts QC: completely redesigned, add support for multi-sample input
    * RNA-seq QC: add new plots (coverage across genebody, reads genomic origin, junction analysis)
    * Command line interface improved
    * BAM QC: changed order of plots
    * BAM QC: fix insert size overflow
    * BAM QC: add insert size info to plain text report
    * BAM QC: fix bug with stats inside of regions
    * BAM QC: performance improved for long genomes
    * RNA-seq QC: fix default protocol 
    * Add utility script to create species annotations for Counts QC
    * Add condtion to exit on error in main script (suggestion from Johan Dahlberg)
    * Fix coverage quota in plain text report (bugfix from John Budde)
    * Major documentation update

A number of renamings in command line interface were introduced.

common:
    -rscriptpath => -R or --rscriptpath
bamqc:
    -outcov => -oc or --output-genome-coverage
rnaseq:
    -counts => -oc
    -algorithm => -a or --algorithm
    -protocol => -p or --sequencing-protocol
comp-counts
    -sorted => -s or --sorted
    -paired => -pe or --paired
    -algorithm => -a or --algorithm
    -protocol => -p or --sequencing-protocol


Version 1.0 - May 29, 2014
--------------------------

    * BAM QC: add computation of mismatches and error rate
    * BAM QC: add new metrics for insert size
    * BAM QC: contig coverage added to text report (patch from Johan Dahlberg)
    * BAM QC: add option to output per-base coverage
    * Compute counts: add full support for paired-end reads
    * RNA-seq QC: add new plots and statistics
    * Updated documentation


Version 0.8.1 - May 5, 2014 
---------------------------
        
    * Fixed bug with CountsQC and R version 3
    * Fixed bug with double reference size computation

Version 0.8 - March 5, 2014 
------------------------------
        
    * Added new analysis mode "RNA-seq QC"
    * Added PDF output path
    * Fixed bug with R-version recognition
    * Fixed bug with per-chromosome standard deviation calculation
    * Fixed integer overflow in number of reads
    * Fixed crash with insert size 
    * Fixed bug with empty SAMRecord

Version 0.7.1 - April 19, 2013 
------------------------------
        
    * Fixed bug with "Coverage Across Reference" plot
    * Fixed bug in BAM QC when regions don't have any coverage 


Version 0.7 - April 10, 2013 
----------------------------

    * Fixed bug in per chromosome coverage report (https://groups.google.com/d/msg/qualimap/AO_6111Gg1E/21SdvEWeAfUJ)
    * Deletions are now properly computed when calculating coverage
    * Fixed bug in renaming of tabs
    * Added global parameter to set Rscript path 
    * Minor fixes in command line tools argument parsing
    * Improved handling of discordant regions between the BED/GFF and BAM files in BAM QC
    * Updated documentation 

Version 0.6 - October 30, 2012 
--------------------------

    * Fixed bug with small homopolymer indels estimation 
    * Fixed bug with EQ and X SAM format tags in BAM QC
    * BAM QC: added option to set minimum homopolymer size
    * BAM QC: added option to open SAM files along with BAM files from GUI

Version 0.5 - July 25, 2012 
--------------------------

    * BED format is now supported throughout application 
    * Raw data from BAM QC and Counts QC plots can be exported
    * BAM QC: Added homopolymer indels estimation
    * BAM QC: Added strand specificity calculation
    * BAM QC: Added clipping profile and number of clipped reads statistics 
    * Compute counts: Added 5' to 3' prime bias calculation 
    * Allow to set java memory size from launching script 
    * Fixed problem with upperCoverageBound in Coverage Across Reference chart 
    * Fixed issue with insert size limits 
    * Added missing qualimap.bat to launch application on MS Windows

Version 0.4 - June 4, 2012 
--------------------------

    * Fixed problem with chromsome limits in Insert Size chart
    * Fixed issue with "compute counts" when regions are having several 
    intersecting reads
    * Fixed crash with Coverage Histogram 0-50X for high coverage samples
    * Fixed Coverage Histogram when minimal coverage is more than zero
    * Fixed issue with GFF files, containing empty lines  


Version 0.3 - May 18,2012
-------------------------

    First public release.


